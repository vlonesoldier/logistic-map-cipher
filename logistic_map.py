from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import base64
import time

class LogisticMapCipher:
    def __init__(self):
        self.x = 0
        self.r = 0
        self.length = 0
        self.key_sequence = []
        self.interval = (2.8, 4)
        self.accuracy = 0.0001
        self.reps = 800
        self.numtoplot = 200
        
    def provide_iv_logistic_map(self):
        self.x = float(input("Enter x_0 value: "))
        self.r = float(input("Enter r value: "))
        self.length = 500 * 500
    
    def generate_logistic_map(self):
        for _ in range(self.length):
            self.x = self.r * self.x * (1 - self.x)
            self.key_sequence.append(int(self.x * 256))
    
    def plot_drawing(self):
        x = np.zeros(self.reps)
        x[0] = (np.random.rand())
        fig, biax = plt.subplots()
        fig.set_size_inches(16, 9)
        
        for r in np.arange(self.interval[0], self.interval[1], self.accuracy):
            for i in range(self.reps - 1):
                x[i + 1] = r * x[i] * (1 - x[i])
                
            biax.plot([r] * self.numtoplot, x[self.reps - self.numtoplot :], c='gray', ls='' , marker=',')
            
        biax.set(xlabel="r", ylabel="x", title="logistic map")
        plt.show()

class TextPermutation(LogisticMapCipher):
    def __init__(self) -> None:
        super().__init__()
        self.str_inp = ""
        self.ciphertext_base64 = ""
        self.key = self.key_sequence
        
    def get_str_inp(self):
        return self.str_inp
    
    def set_str_inp(self, input):
        self.str_inp = input
    
    def enter_input_string(self):
        self.set_str_inp(input("Enter your string: "))
        
    def print_encrypted_text(self):
        ciphertext = self.encrypt_decrypt_text(self.str_inp)
        self.ciphertext_base64 = self.wrap_to_base64(ciphertext)
        print("Encrypted text: ", self.ciphertext_base64)
    
    def print_decrypted_text(self):
        ciphertext_base64_unwrapped = self.unwrap_from_base64(self.ciphertext_base64)
        plaintext = self.encrypt_decrypt_text(ciphertext_base64_unwrapped)
        print("Decrypted text: ", plaintext)

    def encrypt_decrypt_text(self, text):
        encrypted_decrypted_text = ""
        for char, key in zip(text, self.key):
            encrypted_decrypted_char = chr(ord(char) ^ key)
            encrypted_decrypted_text += encrypted_decrypted_char
        return encrypted_decrypted_text

    @staticmethod
    def wrap_to_base64(text_to_wrap):
        text_to_wrap = text_to_wrap.encode('utf-8')
        return base64.b64encode(text_to_wrap).decode('utf-8')
    
    @staticmethod
    def unwrap_from_base64(text_to_unwrap):
        return base64.b64decode(text_to_unwrap).decode()

class ImagePermutation(LogisticMapCipher):
    def __init__(self) -> None:
        super().__init__()
        self.original_img = Image.open("lovestory.jpeg")
        self.height, self.width = self.original_img.size
        self.pixel_map = self.original_img.load()
        self.indices = list(range(self.height))
        self.key = self.key_sequence
    
    def set_pixel_map(self, pixel_map_input):
        self.pixel_map = pixel_map_input
    
    def show_encrypted_image(self):
        start = time.time()

        self.reorder_indices_and_keys()
        self.set_pixel_map(self.shuffle_image())
        self.encrypt_decrypt_image()
        self.original_img.show()
        
        end = time.time()
        print(f"Decryption time: {end - start}")
        
    def show_decrypted_image(self):
        start = time.time()
        
        self.encrypt_decrypt_image()
        self.set_pixel_map(self.reshuffle_image())
        self.original_img.show()
        
        end = time.time()
        print(f"Decryption time: {end - start}")

    
    def encrypt_decrypt_image(self):
        copied_key = self.key.copy()
        z = 0
        
        for i in range(self.height):
            for j in range(self.width):
                self.pixel_map[i, j] = (self.pixel_map[i, j][0] ^ int(copied_key[z]), self.pixel_map[i, j][1] ^ int(copied_key[z]), self.pixel_map[i, j][2] ^ int(copied_key[z]))
                z += 1

    def reorder_indices_and_keys(self):
        for i in range(self.height):
            for j in range(self.width):
                if(self.key[i] > self.key[j]):
                    self.key[i], self.key[j] = self.key[j], self.key[i]
                    self.indices[i], self.indices[j] = self.indices[j], self.indices[i]

    def shuffle_image(self):
        temp_pixel_value = 0

        for _ in range(10):
            for i in reversed(range(self.height)):
                k = self.width - 1
                for j in reversed(range(self.width)):
                    temp_pixel_value = self.pixel_map[i, j]
                    self.pixel_map[i, j] = self.pixel_map[i, self.indices[k]]
                    self.pixel_map[i, self.indices[k]] = temp_pixel_value
                    k = k - 1
        return self.pixel_map

    def reshuffle_image(self):
        temp_pixel_value = 0

        for _ in range(10):
            for i in range(self.height):
                k = 0
                for j in range(self.width):
                    temp_pixel_value = self.pixel_map[i, self.indices[k]]
                    self.pixel_map[i, self.indices[k]] = self.pixel_map[i, j]
                    self.pixel_map[i, j] = temp_pixel_value
                    k = k + 1
        return self.pixel_map

class Launcher(TextPermutation, ImagePermutation):
    def __init__(self):
        super().__init__()
    
    def init_logistic_map(self):
        self.provide_iv_logistic_map()
        self.generate_logistic_map()
    
    def launch_program(self):
        self.menu()
        self.init_logistic_map()

        match self.chosen_option:
            case '1':
                self.plot_drawing()
            case '2':
                self.enter_input_string()
                self.print_encrypted_text()        
                self.print_decrypted_text()
            case '3':
                self.show_encrypted_image()
                
                time.sleep(2)
                print("Would you like to decrypt an image? (y/n): ")
                decision = input()
                
                if(decision == 'y' or decision == 'Y'):
                    self.show_decrypted_image()
                elif(decision == 'n' or decision == 'N'):
                    self.exit_program()
                else:
                    self.exit_program_bad_arg()
    
    def set_chosen_option(self, chosen_option):
        self.chosen_option = chosen_option
    
    def menu(self):
        print("Choose what type of operation you'd like to perform:\n")
        print("1) Show bifurcation diagram of logistic map")
        print("2) Encrypt/decrypt a text")
        print("3) Encrypt an image with logistic map")
        self.set_chosen_option(input())
        self.validate_option()
    
    def validate_option(self):
        try:
            if(0 >= int(self.chosen_option) or int(self.chosen_option) > 3):
                self.exit_program_bad_arg()
        except:
            self.exit_program_bad_arg()

    @staticmethod
    def exit_program_bad_arg():
        print("Bad argument! Exiting the program...")
        exit(0)

    @staticmethod
    def exit_program():
        print("Exiting the program...")
        exit(0)

def generate_bakers_map(x, y, length):
    xkey = []
    ykey = []
    xkey_int = []
    ykey_int = []
    
    for _ in range(length):
        if 0 <= x <= 0.5:
            x = 2 * x
            y = y / 2
        elif 0.5 < x <= 1:
            x = 2 * x - 1
            y = (y / 2) + 0.5
        
        xkey.append(x)
        ykey.append(y)
        
        xkey_int.append(int((x * pow(10, 5))) % 256)
        ykey_int.append(int((y * pow(10, 5))) % 256)
    
    return xkey_int, ykey_int

def main():
    launcher = Launcher()
    launcher.launch_program()
    
if __name__ == '__main__':
    main()

# baker_map = bakers_map_keygen(0.512, 0.42, 10)